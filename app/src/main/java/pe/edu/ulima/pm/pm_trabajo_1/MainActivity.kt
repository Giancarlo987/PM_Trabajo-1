package pe.edu.ulima.pm.pm_trabajo_1

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {
    var player1 : String = "X"
    var player2 : String = "0"
    var turno : Int = 0

    var butBoton1 : Button? = null
    var butBoton2 : Button? = null
    var butBoton3 : Button? = null
    var butBoton4 : Button? = null
    var butBoton5 : Button? = null
    var butBoton6 : Button? = null
    var butBoton7 : Button? = null
    var butBoton8 : Button? = null
    var butBoton9 : Button? = null

    var tviMensaje : TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        butBoton1 = findViewById(R.id.butBoton1)
        butBoton2 = findViewById(R.id.butBoton2)
        butBoton3 = findViewById(R.id.butBoton3)
        butBoton4 = findViewById(R.id.butBoton4)
        butBoton5 = findViewById(R.id.butBoton5)
        butBoton6 = findViewById(R.id.butBoton6)
        butBoton7 = findViewById(R.id.butBoton7)
        butBoton8 = findViewById(R.id.butBoton8)
        butBoton9 = findViewById(R.id.butBoton9)

        tviMensaje = findViewById(R.id.tviMensaje)

        butBoton1!!.setOnClickListener(this)
        butBoton2!!.setOnClickListener(this)
        butBoton3!!.setOnClickListener(this)
        butBoton4!!.setOnClickListener(this)
        butBoton5!!.setOnClickListener(this)
        butBoton6!!.setOnClickListener(this)
        butBoton7!!.setOnClickListener(this)
        butBoton8!!.setOnClickListener(this)
        butBoton9!!.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        var playerTurno : String
        if (turno % 2 == 0){
            playerTurno = player1
        }else{
            playerTurno = player2
        }
        when (v!!.id) {
            R.id.butBoton1 -> {
                if(butBoton1!!.text == "")
                    butBoton1!!.text = playerTurno
            }
            R.id.butBoton2 -> {
                if(butBoton2!!.text == "")
                    butBoton2!!.text = playerTurno
            }
            R.id.butBoton3 -> {
                if(butBoton3!!.text == "")
                    butBoton3!!.text = playerTurno
            }
            R.id.butBoton4 -> {
                if(butBoton4!!.text == "")
                    butBoton4!!.text = playerTurno
            }
            R.id.butBoton5 -> {
                if(butBoton5!!.text == "")
                    butBoton5!!.text = playerTurno
            }
            R.id.butBoton6 -> {
                if(butBoton6!!.text == "")
                    butBoton6!!.text = playerTurno
            }
            R.id.butBoton7 -> {
                if(butBoton7!!.text == "")
                    butBoton7!!.text = playerTurno
            }
            R.id.butBoton8 -> {
                if(butBoton8!!.text == "")
                    butBoton8!!.text = playerTurno
            }
            R.id.butBoton9 -> {
                if(butBoton9!!.text == "")
                    butBoton9!!.text = playerTurno
            }
        }

        tviMensaje!!.text ="Le toca al jugador: $playerTurno"
        turno += 1
    }
}